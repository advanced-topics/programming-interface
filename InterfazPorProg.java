import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JList;

public class InterfazPorProg extends JFrame implements ActionListener {
    JButton btnAccion;
    JButton btnCerrar;

    JTextField tf1;
    JFormattedTextField tf2;
    JList ls1;

    DefaultListModel data;

    public static void main(String[] args) {

        InterfazPorProg frm = new InterfazPorProg("Titulo", 640, 380);
        frm.setVisible(true);
    }

    public InterfazPorProg(String titulo, int ancho, int alto) {
        super(titulo);
        this.setSize(ancho, alto);
        this.setLayout(new FlowLayout());

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.btnAccion = new JButton("Acción");
        this.btnCerrar = new JButton("Cerrar");

        this.btnCerrar.addActionListener(this);
        this.btnAccion.addActionListener(this);

        this.tf1 = new JTextField(40);
        this.tf2 = new JFormattedTextField(100);

        data = new DefaultListModel();
        data.addElement("Dato1");
        data.addElement("Dato2");
        data.addElement("Dato3");

        this.ls1 = new JList(data);

        // Especificar atributos
        this.btnCerrar.setSize(50, 20);
       
        this.add(new JLabel("Nombres"));
        this.add(tf1);
        this.add(new JLabel("Edad"));
        this.add(tf2);
        
        this.add(btnAccion);
        this.add(btnCerrar);
        this.add(ls1);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String mensaje = "";

        if (e.getActionCommand().equals("Cerrar")) {
            System.exit(0);
        } else {
            mensaje = tf1.getText() + " / " + tf2.getText();
            JOptionPane.showMessageDialog(null, mensaje);

        }
    }
}
