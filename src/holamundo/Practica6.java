/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package holamundo;

import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author jairsantos
 */
public class Practica6 extends javax.swing.JFrame {

    /**
     * Creates new form Practica6
     */
    public Practica6() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tfRuta = new javax.swing.JTextField();
        lbMensaje = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        mnNuevo = new javax.swing.JCheckBoxMenuItem();
        mnAbrir = new javax.swing.JCheckBoxMenuItem();
        mnGuardar = new javax.swing.JCheckBoxMenuItem();
        mnCerrar = new javax.swing.JCheckBoxMenuItem();
        jMenu2 = new javax.swing.JMenu();
        mnCortar = new javax.swing.JCheckBoxMenuItem();
        mnPegar = new javax.swing.JCheckBoxMenuItem();
        mnCopiar = new javax.swing.JCheckBoxMenuItem();
        mnSeleccionar = new javax.swing.JCheckBoxMenuItem();
        jMenu3 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tfRuta.setText("jTextField1");

        lbMensaje.setText("jLabel1");

        jMenu1.setText("File");

        mnNuevo.setSelected(true);
        mnNuevo.setText("Nuevo");
        mnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnNuevoActionPerformed(evt);
            }
        });
        jMenu1.add(mnNuevo);
        mnNuevo.getAccessibleContext().setAccessibleDescription("");

        mnAbrir.setSelected(true);
        mnAbrir.setText("Abrir");
        mnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnAbrirActionPerformed(evt);
            }
        });
        jMenu1.add(mnAbrir);

        mnGuardar.setSelected(true);
        mnGuardar.setText("Guardar");
        jMenu1.add(mnGuardar);

        mnCerrar.setSelected(true);
        mnCerrar.setText("Cerrar");
        mnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnCerrarActionPerformed(evt);
            }
        });
        jMenu1.add(mnCerrar);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");

        mnCortar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        mnCortar.setSelected(true);
        mnCortar.setText("Cortar");
        jMenu2.add(mnCortar);

        mnPegar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        mnPegar.setSelected(true);
        mnPegar.setText("Pegar");
        mnPegar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnPegarActionPerformed(evt);
            }
        });
        jMenu2.add(mnPegar);

        mnCopiar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        mnCopiar.setSelected(true);
        mnCopiar.setText("Copiar");
        mnCopiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnCopiarActionPerformed(evt);
            }
        });
        jMenu2.add(mnCopiar);

        mnSeleccionar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        mnSeleccionar.setSelected(true);
        mnSeleccionar.setText("Seleccionar");
        jMenu2.add(mnSeleccionar);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Ayuda");
        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbMensaje)
                    .addComponent(tfRuta, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(47, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addComponent(tfRuta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbMensaje)
                .addContainerGap(135, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnNuevoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnNuevoActionPerformed

    private void mnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnAbrirActionPerformed
        // TODO add your handling code here:
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & GIF Images", "jpg", "gif");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            String archivo = chooser.getSelectedFile().getAbsolutePath();
            this.tfRuta.setText(archivo);

            try {
                FileWriter file = new FileWriter(archivo);
                this.lbMensaje.setText("Archivo creado");
                file.write("Hola");
                file.close();
            } catch (IOException ex) {

                System.Logger.getLogger(Practica4.class.getName()).log(System.Logger.Level.ERROR, null, ex);
                this.lbMensaje.setText("Error: " + ex.getMessage());
            }

        }
    }//GEN-LAST:event_mnAbrirActionPerformed

    private void mnCopiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnCopiarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnCopiarActionPerformed

    private void mnPegarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnPegarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnPegarActionPerformed

    private void mnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnCerrarActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_mnCerrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Practica6.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Practica6.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Practica6.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Practica6.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Practica6().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JLabel lbMensaje;
    private javax.swing.JCheckBoxMenuItem mnAbrir;
    private javax.swing.JCheckBoxMenuItem mnCerrar;
    private javax.swing.JCheckBoxMenuItem mnCopiar;
    private javax.swing.JCheckBoxMenuItem mnCortar;
    private javax.swing.JCheckBoxMenuItem mnGuardar;
    private javax.swing.JCheckBoxMenuItem mnNuevo;
    private javax.swing.JCheckBoxMenuItem mnPegar;
    private javax.swing.JCheckBoxMenuItem mnSeleccionar;
    private javax.swing.JTextField tfRuta;
    // End of variables declaration//GEN-END:variables
}
