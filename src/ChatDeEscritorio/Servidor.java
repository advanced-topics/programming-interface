package ChatDeEscritorio;
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class Servidor {
    ArrayList<Conexion> conexiones;
    ArrayList<String> usersOnline = new ArrayList<String>();
    ServerSocket ss;
    
    String [][] usuarios = {
                            {"hugo",  "123"},
                            {"paco",  "123"},
                            {"luis",  "123"},
                            {"donald","123"}};
    private static final String MESSAGE_SERVER = "message_server";
    private static final String MESSAGE_LOGIN = "message_login";
    private static final String MESSAGE = "message";
    private static final String USERS_ONLINE = "users_online";;
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new Servidor()).start();
            }
        });
    }
    private void start() {
        this.conexiones = new ArrayList<>();
        Socket socket;
        Conexion cnx;

        try {
            ss = new ServerSocket(8999);
            System.out.println("Servidor iniciado, en espera de conexiones");

            while (true){
                socket = ss.accept();
                cnx = new Conexion(this, socket, conexiones.size());
                conexiones.add(cnx);
                cnx.start();
            }
            
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Broadcasting
    private void difundir(String id, String mensaje, String type, String user, Boolean all) {
        Conexion hilo;
        JSONObject message = new JSONObject();
        message.put("type", type);
        message.put("message", mensaje);
        message.put("sender", user);
        if(all)
            for (int i = 0; i < this.conexiones.size(); i++){
                hilo = this.conexiones.get(i);
                if (hilo.cnx.isConnected() && hilo.estado == hilo.CHAT){
                    hilo.enviar(message.toString());
                }
            }
        else
            for (int i = 0; i < this.conexiones.size(); i++){
                hilo = this.conexiones.get(i);
                if (hilo.cnx.isConnected() && hilo.estado == hilo.CHAT && !id.equals(hilo.id)){
                    hilo.enviar(message.toString());
                }
            }
    }
    private void shareUsersOnline(String id) {
        Conexion hilo;
        JSONObject message = new JSONObject();
        message.put("type", USERS_ONLINE);
        JSONArray a = new JSONArray(usersOnline);
        message.put("users_online", a);
        for (int i = 0; i < this.conexiones.size(); i++){
            hilo = this.conexiones.get(i);
            if (hilo.cnx.isConnected() && hilo.estado == hilo.CHAT){
                hilo.enviar(message.toString());
            }
        }
    }
    
    class Conexion extends Thread {
        BufferedReader in;
        PrintWriter    out;
        Socket cnx;
        Servidor padre;
        int numCnx = -1;
        String id = "";
        public final int SIN_USER   = 0;
        public final int USER_IDENT = 1;
        public final int CHAT       = 4;
        int estado = SIN_USER;
        public Conexion(Servidor padre, Socket socket, int num){
            this.cnx = socket;
            this.padre = padre;
            this.numCnx = num;
            this.id = socket.getInetAddress().getHostAddress()+num;
        }

        @Override
        public void run() {
            String linea = "", username = "", pass = "";
            JSONObject user, message;
            
            boolean ejecutar = true;
            try {
                in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
                out = new PrintWriter(cnx.getOutputStream(),true);
                System.out.printf("Aceptando conexion desde %s\n", cnx.getInetAddress().getHostAddress());
                while(estado != CHAT && ejecutar){
                    switch(estado){
                        case SIN_USER:
                            estado = USER_IDENT;
                            break;
                        case USER_IDENT:
                            linea = in.readLine();
                            user = new JSONObject(linea);
                            if(user.has("username") && user.has("password")){
                                username = user.getString("username");
                                pass = user.getString("password");
                                boolean found = false;
                                for (int i=0; i < usuarios.length; i++){
                                    if (username.equals(usuarios[i][0]) && pass.equals(usuarios[i][1])){
                                        found = true;
                                    }
                                }
                                if (!found){
                                    statusLogin("Usuario o contraseña incorrecta, intente de nuevo", false);
                                    ejecutar = false;
                                    cnx.close();
                                }
                                else{
                                    estado = CHAT;
                                    statusLogin("Inicio de sesión exitoso", true);
                                }
                            }
                            break;
                    }
                }
                if(ejecutar){
                    this.padre.difundir(id, username + " ha entrado en el chat.", MESSAGE_SERVER, username, true);
                    usersOnline.add(username);
                    this.padre.shareUsersOnline(id);
                    System.out.printf("[%s] %s se ha logueado\n", cnx.getInetAddress().getHostAddress(), username);
                    while((linea = in.readLine()) != null){
                        message = new JSONObject(linea);
                        String type = message.getString("type");
                        if(type.equals(MESSAGE)){
                            if(!message.getString("message").equals("/salir")){
                                System.out.printf("[%s] %s: %s\n", cnx.getInetAddress().getHostAddress(), username, linea);
                                this.padre.difundir(this.id, message.getString("message"), MESSAGE, username, false);
                            }else{
                                this.padre.difundir(id, username + " ha salido del chat.", MESSAGE_SERVER, username, true);
                                System.out.printf("[%s] %s ha abandonado el chat.\n", cnx.getInetAddress().getHostAddress(), username);
                                estado = SIN_USER;
                                usersOnline.remove(username);
                                this.padre.shareUsersOnline(id);
                                ejecutar = false;
                                cnx.close();
                                break;
                            }
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        private void enviar(String mensaje) {
            this.out.println(mensaje);
        }
        private void statusLogin(String mensaje, boolean online){
            JSONObject message = new JSONObject();
            message.put("type", MESSAGE_LOGIN);
            message.put("online", online);
            message.put("message", mensaje);
            enviar(message.toString());
        }
    }
}
