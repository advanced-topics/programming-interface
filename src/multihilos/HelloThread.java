/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package multihilos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jairsantos
 */
public class HelloThread extends Thread {

    String cadena = "";
    int seg = 1000;
    String nom = "";

    HelloThread(String nom, int seg) {
        this.seg = seg;
        this.nom = nom;
    }

    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(this.nom + "-> Hello from a thread! " + i);
            this.cadena = this.nom;

            try {
                Thread.sleep(seg * 1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(HelloRunnable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static void main(String args[]) {
        
        HelloThread t1 = new HelloThread("Hilo 1", 3);
        t1.setPriority(Thread.NORM_PRIORITY);
        HelloThread t2 = new HelloThread("Hilo 2", 2);
        t2.setPriority(Thread.NORM_PRIORITY);
        HelloThread t3 = new HelloThread("Hilo 3", 1);
        t3.setPriority(Thread.NORM_PRIORITY);
        
        t1.start();
        t2.start();
        t3.start();
    }

}
