/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package multihilos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jairsantos
 */
public class HelloRunnable implements Runnable {

    String cadena = "";
    int seg = 1000;
    String nom = "";

    HelloRunnable(String nom, int seg) {
        this.seg = seg;
        this.nom = nom;
    }

    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(this.nom+ "-> Hello from a thread! " + i);
            this.cadena = this.nom;
            
            try {
                Thread.sleep(seg * 1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(HelloRunnable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static void main(String args[]) {
        (new Thread(new HelloRunnable("Hilo 1", 3))).start();
        (new Thread(new HelloRunnable("Hilo 2", 2))).start();
        (new Thread(new HelloRunnable("Hilo 3", 1))).start();
    }

}
